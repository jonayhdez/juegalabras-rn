import React, { Component } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

function Cell(props) {
    const { value, solved, position } = props.data;
    return (
        <TouchableOpacity style={styles.cell}>
            <Text numberOfLines={1} style={[styles.text, solved ? styles.solvedCell : styles.unsolvedCell]}>{solved ? value : position}</Text>
        </TouchableOpacity>
    )
}
export default class GameBoard extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const data = [];
        this.props.data.forEach((element) => {
            data.push({ ...element, solved: this.props.status == BoardStates.showAll ? true : element.solved })

        });
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <Cell data={data[0]} />
                    <Cell data={data[1]} />
                    <Cell data={data[2]} />
                </View>
                <View style={styles.row}>
                    <Cell data={data[3]} />
                    <Cell data={data[4]} />
                    <Cell data={data[5]} />
                </View>
                <View style={styles.row}>
                    <Cell data={data[6]} />
                    <Cell data={data[7]} />
                    <Cell data={data[8]} />
                </View>
            </View >
        )
    }
}
export const BoardStates = {
    showAll: "showAll", // Que muestre las palabras en todas las casillas independientemente del valor de sus datos
    fromData: "fromData", // Que muestre las casillas en función del valor de sus datos
}

export const dummyData = [
    {
        "position": 1,
        "solved": false,
        "value": "V1"
    },
    {
        "position": 2,
        "solved": false,
        "value": "V2"
    },
    {
        "position": 3,
        "solved": false,
        "value": "V3"
    },
    {
        "position": 4,
        "solved": false,
        "value": "V4"
    },
    {
        "position": 5,
        "solved": false,
        "value": "V5"
    },
    {
        "position": 6,
        "solved": false,
        "value": "V6"
    },
    {
        "position": 7,
        "solved": false,
        "value": "V7"
    },
    {
        "position": 8,
        "solved": false,
        "value": "V8"
    },
    {
        "position": 9,
        "solved": false,
        "value": "V9"
    }
]
const styles = StyleSheet.create({
    container: { flex: 1, padding: 0, marginHorizontal: 10 },
    row: { padding: 0, flexDirection: 'row', justifyContent: "space-between" },
    cell: { flex: 1, height: 50, borderRadius: 15, borderWidth: 1, margin: 5, borderColor: 'yellow', backgroundColor: 'yellow', justifyContent: 'center', marginVertical: 15, alignItems: 'center' },
    text: { fontSize: 20, fontWeight: 'bold', margin: 5, textAlign: 'center', textAlignVertical: 'center' },
    solvedCell: { fontSize: 12 },
    unsolvedCell: {}
});