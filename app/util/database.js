export default [
    {
        name: 'numbers',
        title: "Números",
        onlyForAdult: false,
        data: ["uno", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve", "diez"]
    },
    {
        name: 'names',
        title: "Nombres",
        onlyForAdult: false,
        data: ["Carlos", "Gonzalo", "Laura", "Alejandro", "Eva", "Alberto", "Andrés", "Manuel", "Jonay", "Paula", "Ivan"]
    },
    {
        name: 'months',
        title: "Meses",
        onlyForAdult: false,
        data: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
    },
    {
        name: 'curse',
        title: "Palabrotas",
        onlyForAdult: true,
        data: ["Caca", "Culo", "Pedo", "Pis", "Desgraciado", "Inutil", "Bobo", "Soez", "Abrazafarolas", "Baboso", "Caraculo", "Fantoche", "Gañán", "Huevón", "Lechugino"]
    },

]