import { Dimensions } from 'react-native';
// https://stackoverflow.com/questions/67407857/screen-orientation-on-react-native
const msp = (dim, limit) => {
    return dim.scale * dim.width >= limit || dim.scale * dim.height >= limit;
};

const isPortrait = () => {
    const dim = Dimensions.get('screen');
    return dim.height >= dim.width;
};

const isLandscape = () => {
    const dim = Dimensions.get('screen');
    return dim.width >= dim.height;
};

const isTablet = () => {
    const dim = Dimensions.get('screen');
    return (
        (dim.scale < 2 && msp(dim, 1000)) || (dim.scale >= 2 && msp(dim, 1900))
    );
};

const isPhone = () => {
    return !isTablet();
};

export default {
    isPortrait,
    isLandscape,
    isTablet,
    isPhone,
};