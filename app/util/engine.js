import sourceData from './database'

/**
 * Selecciona aleatoriamente un "paquete de palabras" para el juego, teniendo en cuenta si hay que contemplar que puede devolver contenido sólo para adultos o no
 * 
 * @param  allowAdultContent 
 * @returns 
 */
function selectSource(allowAdultContent) {
    console.log("AllowAdultContent:" + allowAdultContent);

    while (true) {
        let position = Math.floor(Math.random() * sourceData.length)
        let value = sourceData[position];
        if (allowAdultContent === true || value.onlyForAdult === false) {
            console.log("value.onlyForAdult :" + value.onlyForAdult)
            return value;
        }
    }
}

/** Devuelve la estructura de datos necesaria para representar el estado del juego dentro de una store redux */
function createGameDataFromSource(fullData) {
    let selected = []
    // Selecciona nueve elementos distintos
    while (selected.length < 9) {
        let position = Math.floor(Math.random() * fullData.data.length)
        if (!selected.includes(fullData.data[position])) {
            selected.push(fullData.data[position])
        }
    }
    // Por cada elemento seleccionado creamos la estructura
    const result = {
        name: fullData.title,
        data: []
    }
    selected.forEach((element, index) => {
        result.data.push({ position: index + 1, solved: false, value: element })
    })
    console.log('result: ',result)
    return result;
}

/**
 * Crea el contenido del trablero. Inicia un juego
 */
function init(allowAdultContent) {
    return createGameDataFromSource(selectSource(allowAdultContent))
}

export default { init }