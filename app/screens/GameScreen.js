import {
    StyleSheet,
    Text,
    View
} from 'react-native';
import React, {useEffect, useState} from 'react';
import GameBoard, {BoardStates, dummyData} from '../components/Board';
import {useHeaderHeight} from '@react-navigation/stack';
import Orientation from '../util/orientation';
import {Dimensions} from 'react-native';
import {CountdownCircleTimer} from 'react-native-countdown-circle-timer';
import {useDispatch} from "react-redux";
import {initGame} from "../state/gameSlice";

/**
 * Muestra el timer que permite ver todas las palabras descubiertas durante un tiempo determinado
 * @param {*} duration Número de segundos que se debe mostrar
 * @param {*} onComplete Función a la que debe llamar cuando terminen los segundos
 * @returns
 */
function beginGameTimer(duration, onComplete) {
    let colors =
        [['#004777', 0.33],
            ['#F7B801', 0.33],
            ['#A30000', 0.33],
        ]
    return <CountdownCircleTimer
        isPlaying
        duration={duration}
        colors={colors}
        size={100}
        onComplete={() => {
            onComplete()
        }}
    >
        {({remainingTime}) =>
            <Text>{remainingTime}</Text>
        }
    </CountdownCircleTimer>
}

export default function GameScreen() {
    const dispatch = useDispatch();

    // Control de orientación
    const headerHeight = useHeaderHeight();
    const [orientation, setOrientation] = useState(
        Orientation.isPortrait() ? 'portrait' : 'landscape'
    );

    useEffect(() => {
        Dimensions.addEventListener('change', () => {
            setOrientation(Orientation.isPortrait() ? 'portrait' : 'landscape');
        });

        const test = dispatch(initGame(true))
        console.log('test: ', test)
    }, []);


    // Los estilos de la vista cambian en función de la orientación
    let containerStyle = {...styles.container, paddingTop: headerHeight}
    const beginGameContainerStyle = {alignItems: 'center', paddingBottom: 10}
    if (orientation == "landscape") {
        containerStyle = {
            ...containerStyle, flexDirection: "row", paddingLeft: headerHeight, alignContent: 'center'
        };
        beginGameContainerStyle.width = 200;
    }

    return (
        <View style={containerStyle}>


            <View style={beginGameContainerStyle}>
                {beginGameTimer(10, () => {
                })}
                <Text>¡¡¡ Aprovecha para memorizar las palabras !!!</Text>
            </View>

            <View style={{flex: 1}}>
                <GameBoard style={{flex: 1}}
                           status={BoardStates.fromData}
                           data={dummyData} onFinish={() => {
                    // De momento aquí nada
                }}/>
                <View style={{justifyContent: 'space-around'}}>
                    {/* TODO: Hay que mostrar el título del juego seleccionado (nombre del listado de palabras*/}
                    <View style={styles.titleContainer}><Text style={styles.title}>*Nombre del tablero*</Text></View>
                </View>
            </View>

        </View>
    )

}
const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: "flex-end",
        alignItems: "center"
    },
    container: {
        flex: 1, backgroundColor: 'bisque', alignContent: 'center'
    },
    titleContainer: {
        textAlign: 'center',
        textAlignVertical: 'center',

        padding: 10,
        backgroundColor: 'firebrick',
        alignSelf: 'center',
        borderRadius: 20,
        borderColor: 'bisque',
        borderWidth: 1,
        marginBottom: 20
    },
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        color: 'white',
        margin: 5,
    }
})
