import React from "react";
import {StyleSheet, Text, View} from "react-native";
import InputSpinner from "react-native-input-spinner";
import {useDispatch, useSelector} from "react-redux";
import {changeErrorTime, changeLimitTime, changeStartTime} from "../state/configSlice";

export const GameSetupScreen = () => {
    const dispatch = useDispatch();
    const startTime = useSelector(state => state.config.startTime);
    const errorTime = useSelector(state => state.config.errorTime);
    const limitTime = useSelector(state => state.config.limitTime);

    return (
        <View style={styles.container}>
            <View style={styles.containerOptions}>
                <Text style={styles.title}>Tiempo inicial</Text>
                <Text>Tiempo durante el que se mostrarán todas las palabras al comenzar el juego</Text>
                <InputSpinner
                    style={styles.spinner}
                    value={startTime}
                    min={0}
                    step={10}
                    onIncrease={()=>dispatch(changeStartTime('more'))}
                    onDecrease={()=>dispatch(changeStartTime('less'))}
                />
            </View>
            <View style={styles.containerOptions}>
                <Text style={styles.title}>Tiempo de error</Text>
                <Text>Tiempo durante el que se mostrarán todas las palabras al comenzar el juego</Text>
                <InputSpinner
                    style={styles.spinner}
                    value={errorTime}
                    min={0}
                    step={2}
                    onIncrease={()=>dispatch(changeErrorTime('more'))}
                    onDecrease={()=>dispatch(changeErrorTime('less'))}
                />
            </View>
            <View style={styles.containerOptions}>
                <Text style={styles.title}>Limite de juego</Text>
                <Text>Tiempo durante el que se mostrarán todas las palabras al comenzar el juego</Text>
                <InputSpinner
                    style={styles.spinner}
                    value={limitTime}
                    min={0}
                    step={30}
                    onIncrease={()=>dispatch(changeLimitTime('more'))}
                    onDecrease={()=>dispatch(changeLimitTime('less'))}
                />
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'yellow'
    },
    containerOptions: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: '80%'
    },
    spinner:{
        width: '50%'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold'
    },
})