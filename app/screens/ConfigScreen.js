import React from "react";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

import {UserScreen} from "./UserScreen";
import {GameSetupScreen} from "./GameSetupScreen";

const Tab = createBottomTabNavigator();

export const ConfigScreen = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen
                name={'User'}
                component={UserScreen}
                options={
                    {
                        tabBarIcon: ({color, size}) => <MaterialCommunityIcons name={'account'} solid color={color} size={size}/>
                    }
                }
            />
            <Tab.Screen
                name={'GameSetup'}
                component={GameSetupScreen}
                options={
                    {
                        title: 'Config', headerBackTitleVisible: false,
                        tabBarIcon: ({color, size}) => <MaterialCommunityIcons name={'cog'} solid color={color} size={size}/>
                    }
                }
            />
        </Tab.Navigator>
    )
};
