import React, {useEffect, useState} from "react";
import {StyleSheet, Text, View} from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import BouncyCheckbox from "react-native-bouncy-checkbox";
import {useSelector, useDispatch} from "react-redux";
import {setUserType} from "../state/userSlice";

export const UserScreen = () => {
    const dispatch = useDispatch();
    const userType = useSelector(state => state.user.userType);

    const handleAdult = (type) => {
        dispatch(setUserType(type))
    }

    return (
        <View style={styles.container}>
            <View style={styles.containerText}>
                <Text style={styles.title}>Tipo de usuario</Text>
                <Text style={{textAlign: 'center'}}>Se mostrarán listados de palabras en función de la edad del
                    usuario</Text>
            </View>
            <View style={styles.iconsContainer}>
                <View style={styles.icons}>
                    <MaterialCommunityIcons
                        onPress={() => handleAdult('child')}
                        name={'human-child'}
                        solid
                        size={150}
                        color={(userType === 'child') ? 'red' : 'black'}/>
                </View>
                <View style={styles.icons}>
                    <MaterialCommunityIcons
                        onPress={() => handleAdult('adult')}
                        name={'human-male'}
                        solid
                        size={250}
                        color={(userType === 'adult') ? 'red' : 'black'}/>
                </View>

            </View>
            <View style={styles.checkContainer}>
                <BouncyCheckbox
                    size={25}
                    fillColor="red"
                    text="Recordar último nombre de usuario"
                    iconStyle={{borderColor: "red"}}
                    textStyle={{fontFamily: "JosefinSans-Regular"}}
                    onPress={() => alert('pulsado')}
                />
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'pink'
    },
    containerText: {
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 25,
        fontWeight: 'bold'
    },
    iconsContainer: {
        justifyContent: 'center',
        alignItems: 'flex-end',
        height: '50%',
        // width: '100%',
        flexDirection: 'row'
    },
    icons: {
        alignItems: 'flex-end',
        width: '60%'
    },
    checkContainer: {
        height: '20%',
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    }
})