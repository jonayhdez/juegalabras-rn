import React, {useEffect} from 'react';
import {Image, ImageBackground, StyleSheet, Text, TouchableHighlight, View} from "react-native";
import {useDispatch} from "react-redux";
import {clearGame} from "../state/gameSlice";

export const HomeScreen = ({navigation}) => {
    const dispatch=useDispatch();

    useEffect(()=>{
        dispatch(clearGame())
    },[]);

    const play = () => {
        navigation.navigate('Game');
        console.log('game')
    }

    const config = () => {
        navigation.navigate('Config');
    }
    return (
        <ImageBackground style={styles.main} source={require('../assets/background.jpg')}>
            <View style={styles.logoContainer}>
                <Image style={styles.logo} source={require('../assets/logo.png')}/>
                <Text style={[styles.text, {color: 'black'}]}>Juegalabra!!</Text>
            </View>
            <View style={styles.buttonContainer}>
                <TouchableHighlight style={styles.button} onPress={play}>
                    <Text style={styles.text}>Jugar</Text>
                </TouchableHighlight>
                <TouchableHighlight style={[styles.button, {backgroundColor: "#008b8b",}]} onPress={config}>
                    <Text style={styles.text}>Configuracion</Text>
                </TouchableHighlight>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column"
    },
    main: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "flex-end"
    },
    buttonContainer: {
        position: 'absolute',
        width: '100%',
        bottom: 0
    },
    button: {
        backgroundColor: "#dc143c",
        alignItems: "center",
        justifyContent: 'center',
        height: 55
    },
    text: {
        color: 'white',
        fontSize: 20,
    },
    logo: {
        resizeMode: "center",
        height: 150,
        // width: '50%',
    },
    logoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});