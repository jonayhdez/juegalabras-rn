import { combineReducers, configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import userSlideReducer from "./userSlice";
import configSlideReducer from './configSlice';
import {
    persistStore,persistReducer,FLUSH,
    REHYDRATE,PAUSE,PERSIST,PURGE,REGISTER,
} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

const persistConfig = {
    key: 'root',
    version: 1,
    storage: AsyncStorage,
}

const rootReducer = combineReducers({
    user: userSlideReducer,
    config: configSlideReducer
})

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = configureStore({
    reducer: persistedReducer,
    middleware: getDefaultMiddleware({
        serializableCheck: {
            ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
        },
    }),
});
export const persistor = persistStore(store)
export default store;