import { createSlice } from "@reduxjs/toolkit";

export const configSlice = createSlice({
    name: 'config',
    initialState: {
        startTime: 10,
        errorTime: 2,
        limitTime: 60
    },
    reducers: {
        changeStartTime: (state, action) => {
            if (action.payload === 'more'){
                state.startTime += 10
            } else if (action.payload === 'less'){
                state.startTime -= 10
            }
        },
        changeErrorTime: (state, action) => {
            if (action.payload === 'more'){
                state.errorTime += 2
            } else if (action.payload === 'less'){
                state.errorTime -= 2
            }
        },
        changeLimitTime: (state, action) => {
            if (action.payload === 'more'){
                state.limitTime += 30
            } else if (action.payload === 'less'){
                state.limitTime -= 30
            }
        },
    }
});

export const {changeErrorTime, changeLimitTime, changeStartTime} = configSlice.actions;
export default configSlice.reducer;