import { createSlice } from "@reduxjs/toolkit";
import init from '../util/engine'

export const gameSlice = createSlice({
    name: 'game',
    initialState: {
        status: 'empty',
        data: {
            name: null,
            data: [],
        }
    },
    reducers: {
        clearGame: (state, action) => {
            state.status.reset()
        },
        initGame: (state, action) => {
            state.status = 'init';
            init(action.payload);
        }
    }
});

export const {clearGame, initGame} = gameSlice.actions;
export default gameSlice.reducer;