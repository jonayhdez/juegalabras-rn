import React from 'react';
import 'react-native-gesture-handler';
import {Provider} from "react-redux";
import store, {persistor} from "./state/store";

import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";

import {HomeScreen} from './screens/HomeScreen';
import GameScreen from "./screens/GameScreen";
import {ConfigScreen} from "./screens/ConfigScreen";
import {PersistGate} from "redux-persist/integration/react";

const Stack = createStackNavigator();

const App = () => {

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <NavigationContainer>
                    <Stack.Navigator>
                        <Stack.Screen name={'Home'} component={HomeScreen} options={{title: '', headerShown: false}}/>
                        <Stack.Screen name={'Game'} component={GameScreen} options={{headerBackTitleVisible: false}}/>
                        <Stack.Screen name={'Config'} component={ConfigScreen}
                                      options={{title: 'Setup', headerBackTitleVisible: false}}/>
                    </Stack.Navigator>
                </NavigationContainer>
            </PersistGate>
        </Provider>

    );
}

export default App;
